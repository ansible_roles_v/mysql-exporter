MySQL Exporter
==============
Installs and configures MySQL Prometheus Exporter.  

Include role
------------
```yaml
- name: mysql_exporter  
  src: https://gitlab.com/ansible_roles_v/mysql-exporter/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - mysql_exporter
```